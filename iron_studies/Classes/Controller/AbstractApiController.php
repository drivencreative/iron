<?php
namespace Netfed\IronStudies\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use Netfed\IronStudies\Mvc\View\JsonView;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class AbstractRestController
 *
 */
abstract class AbstractApiController extends ActionController
{

    /**
     * Configuration for JsonView
     **/
    protected $model = [];

    /**
     * The default view object to use if none of the resolved views can render
     * a response for the current request.
     *
     * @var string
     * @api
     */
    protected $defaultViewObjectName = \TYPO3\CMS\Fluid\View\TemplateView::class;

    /**
     * A list of formats and object names of the views which should render them.
     *
     * @var array
     */
    protected $viewFormatToObjectNameMap = [
        'html' => \TYPO3\CMS\Fluid\View\TemplateView::class,
        'json' => JsonView::class
    ];

    /**
     * Check if is JsonView and add Configuration and Variables
     *
     * @param array $configuration
     * @param array $variablesToRender
     *
     * @return void
     **/
    protected function jsonView($configuration, $variablesToRender = []) {
        if ($this->request->getFormat() == 'json') {
            $this->view->setConfiguration($configuration);
            $this->view->setVariablesToRender($variablesToRender ?: array_keys($configuration));
        }
    }

    /**
     * action initialize
     *
     * @return void
     */
    protected function initializeView(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface $view = null)
    {
        if (in_array($this->actionMethodName, ['listAction'])) {
            $this->model = ['_descendAll' => $this->model];
        }
        $this->jsonView(['data' => $this->model]);

    }

}
