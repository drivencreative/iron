<?php

namespace Netfed\IronStudies\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * OrderController
 */
class OrderController extends AbstractApiController
{

    /**
     * Initialize Action
     */
    public function initializeAction()
    {
        if ($this->configuration = reset($this->arguments)) {
            $this->configuration = $this->configuration->getPropertyMappingConfiguration()
                ->allowAllProperties()
                ->skipUnknownProperties()
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                    TRUE
                )
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                    TRUE
                );
        }
    }

    /**
     * Initialize process
     */
    public function initializeProcessAction()
    {
        $this->configuration->forProperty('study')->setTypeConverter(
            $this->objectManager->get(\Netfed\IronStudies\Domain\TypeConverter\RelationConverter::class)
        );
    }

    /**
     * process list
     *
     * @return void
     */
    public function processAction(\Netfed\IronStudies\Domain\Model\Order $order)
    {

        $this->sendTemplatedMail(
            $this->settings['receiver'],
            $this->settings['sender'],
            $this->settings['subject'],
            ['order' => $order],
            'Mail/Admin'
        );

        if ($studies = $order->getStudy()) {
            $fileName = '';
            $file = tempnam("tmp", "zip");
            /** @var \ZipArchive $zip **/
            $zip = $this->objectManager->get(\ZipArchive::class);
            $zip->open($file, \ZipArchive::OVERWRITE);

            $pdf = '';
            foreach ($studies as $study) {
                if ($study->getDocument()) {
                    $fileName = $order->getStudy()->count() > 1 ? 'Download' : $study->getDocument()->getOriginalResource()->getName();
                    $pdf = $study->getDocument()->getOriginalResource()->getPublicUrl();
                    $zip->addFile($pdf, $study->getDocument()->getOriginalResource()->getName());
                }
            }

            $type = 'zip';
            if ($zip->numFiles == 1 && $pdf) {
                $file = $pdf;
                $type = 'pdf';
            }

            $zip->close();

            if (is_readable($file)) {
                $this->response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
                $this->response->setHeader('Content-Type', 'application/' . $type);
                $this->response->setHeader('Pragma', 'no-cache');
                $this->response->setHeader('Expires', '0');

                $this->response->setContent(readfile($file));

                $this->response->send();
                exit();
            }
        }
        $this->redirect('list', 'Study', 'ironstudies', [], $this->settings['listPid'] ?: $GLOBALS['TSFE']->id);
    }

    /**
     * Create HTML from variables and template path, send email
     *
     * @param string $receiver Email receiver
     * @param string $sender Email sender
     * @param string $subject Email subject
     * @param array $variables objects passed to view
     * @param string $template file name of partial template
     * @param string|array $attachment files to be attached
     * @param string $css file path of css
     */
    protected function sendTemplatedMail($receiver, $sender, $subject, $variables, $template, $attachment = NULL)
    {
        $messageBody = $this->getStandaloneView($variables, $template);
        $this->sendEmail($receiver, $sender, $subject, $messageBody, $attachment);
    }

    /**
     * render fluid standalone view
     *
     * @param array $variables objects passed to view
     * @param string $template file name of partial template
     *
     * @return string
     */
    protected function getStandaloneView($variables, $template)
    {
        /** @var $standaloneView \TYPO3\CMS\Fluid\View\StandaloneView * */
        $standaloneView = $this->objectManager->get(\TYPO3\CMS\Fluid\View\StandaloneView::class);
        $standaloneView->setControllerContext($this->controllerContext);
        $standaloneView->getRequest()->setControllerExtensionName($this->extensionName);
        $standaloneView->setFormat('html');
        $standaloneView->setLayoutRootPaths($this->settings['view']['layoutRootPaths']);
        $standaloneView->setTemplateRootPaths($this->settings['view']['templateRootPaths']);
        $standaloneView->setPartialRootPaths($this->settings['view']['partialRootPaths']);
        $standaloneView->setTemplate($template);
        $standaloneView->assignMultiple($variables);

        return $standaloneView->render();
    }

    /**
     * Send mail
     *
     * @param string|array $receiver
     * @param string|array $sender
     * @param string $subject
     * @param string $messageBody
     * @param mixed $attachment
     *
     * @return bool
     */
    protected function sendEmail($receiver, $sender, $subject, $messageBody, $attachment = NULL)
    {
        /** @var $message \TYPO3\CMS\Core\Mail\MailMessage * */
        $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        $message->setTo($receiver)->setFrom($sender)->setSubject($subject);
        $message->setBody($messageBody, 'text/html');

        if ($attachment) {
            $attachment = is_array($attachment) ? $attachment : [$attachment];
            foreach ($attachment as $attach) {
                $message->attach(\Swift_Attachment::fromPath($attach['url'] ?: $attach));
            }
        }

        $message->send();

        return $message->isSent();
    }

}