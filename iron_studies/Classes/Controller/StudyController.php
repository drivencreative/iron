<?php
namespace Netfed\IronStudies\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * StudyController
 */
class StudyController extends AbstractApiController
{
    /**
     * Configuration for JsonView
     **/
    protected $model = [
        '_exclude' => ['pid'],
    ];

    /**
     * studyRepository
     *
     * @var \Netfed\IronStudies\Domain\Repository\StudyRepository
     * @inject
     */
    protected $studyRepository = NULL;
    
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {

//        setlocale(LC_TIME, 'de_DE', 'de_DE.UTF-8');
//        DebuggerUtility::var_dump(strftime("%A, %d. %B %Y", strtotime('15:35 26-03-2006')));
        if ($this->request->getFormat()=='json'){
            $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
            $querySettings->setRespectStoragePage(FALSE);
            $this->studyRepository->setDefaultQuerySettings($querySettings);
        }
        $studies = $this->settings['limit'] ? $this->studyRepository->findAllLimited((int)$this->settings['limit']) : $this->studyRepository->findAll();
        $this->view->assign('contentUid', $this->configurationManager->getContentObject()->data['uid']);
        $this->view->assign('data', $studies);
        $this->view->assign('count',  $this->studyRepository->countAll());
    }
    
    /**
     * action show
     *
     * @param \Netfed\IronStudies\Domain\Model\Study $study
     * @return void
     */
    public function showAction(\Netfed\IronStudies\Domain\Model\Study $study)
    {
        $this->view->assign('study', $study);
    }

}