<?php
namespace Netfed\IronStudies\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Order
 */
class Order extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * study
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\IronStudies\Domain\Model\Study>
     */
    protected $study = null;

    /**
     * @var string
     * @validate NotEmpty
     */
    protected $lastName = '';

    /**
     * @var string
     * @validate NotEmpty
     */
    protected $firstName = '';

    /**
     * @var string
     * @validate NotEmpty
     */
    protected $email = '';

    /**
     * @var string
     */
    protected $company = '';

    /**
     * @var string
     */
    protected $message = '';

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getFirstName ()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName ($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getEmail ()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail ($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the study
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\IronStudies\Domain\Model\Study> $study
     */
    public function getStudy()
    {
        return $this->study;
    }

    /**
     * Sets the study
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\IronStudies\Domain\Model\Study> $study
     * @return void
     */
    public function setStudy(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $study = null)
    {
        $this->study = $study;
    }

    /**
     * @return string
     */
    public function getCompany ()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany ($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getMessage ()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage ($message)
    {
        $this->message = $message;
    }

}
