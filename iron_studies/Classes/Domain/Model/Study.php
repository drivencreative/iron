<?php
namespace Netfed\IronStudies\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Study
 */
class Study extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     */
    protected $title = '';
    
    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $image = null;

    /**
     * document
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $document = null;

    /**
     * text
     *
     * @var string
     */
    protected $text = '';
    
    /**
     * date
     *
     * @var \DateTime
     */
    protected $date = null;

    /**
     * link
     *
     * @var string
     */
    protected $link = '';

    /**
     * additionalLink
     *
     * @var string
     */
    protected $additionalLink = null;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImageSrc()
    {
        $settings = [];
        if ($this->image) {
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
            $configurationManager = $objectManager->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class);
            $settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);

            /** @var $imageService \TYPO3\CMS\Extbase\Service\ImageService **/
            $imageService = $objectManager->get(\TYPO3\CMS\Extbase\Service\ImageService::class);
            try {
                $image = $imageService->getImage($this->image->getUid(), $this->image, 0);

                $cropString = $image->hasProperty('crop') ? $image->getProperty('crop') : '';
                $cropVariantCollection = CropVariantCollection::create((string)$cropString);
                $cropVariant = $settings['cropVariant'] ?: 'default';
                $cropArea = $cropVariantCollection->getCropArea($cropVariant);
                $processingInstructions = [
//                    'width' => $this->arguments['width'],
//                    'height' => $this->arguments['height'],
//                    'minWidth' => $this->arguments['minWidth'],
//                    'minHeight' => $this->arguments['minHeight'],
                    'maxWidth' => $settings['width'],
//                    'maxHeight' => $this->arguments['maxHeight'],
                    'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
                ];
                $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
                $imageUri = $imageService->getImageUri($processedImage, 1);

                return $imageUri;
            } catch (\TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException $e) {
                // thrown if file does not exist
            } catch (\UnexpectedValueException $e) {
                // thrown if a file has been replaced with a folder
            } catch (\RuntimeException $e) {
                // RuntimeException thrown if a file is outside of a storage
            } catch (\InvalidArgumentException $e) {
                // thrown if file storage does not exist
            }
        }
    }
    
    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the document
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Sets the document
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $document
     * @return void
     */
    public function setDocument(\TYPO3\CMS\Extbase\Domain\Model\FileReference $document)
    {
        $this->document = $document;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }
    
    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }
    
    /**
     * Returns the date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Returns the date
     *
     * @return string
     */
    public function getDateFormated()
    {
        return $this->date ? $this->date->format('F Y'): '';
    }

    /**
     * Sets the date
     *
     * @param \DateTime $date
     * @return void
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Returns the link
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Returns the link
     *
     * @return string $link
     */
    public function getUri()
    {
        $contentObject = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer::class);
        $contentObject->start([], '');
        return $this->link ? $contentObject->getTypoLink_URL($this->link) : '';
    }

    /**
     * Sets the link
     *
     * @param string $link
     * @return void
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * Returns the additionalLink
     *
     * @return string $additionalLink
     */
    public function getAdditionalLink()
    {
        $contentObject = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer::class);
        $contentObject->start([], '');
        if($this->additionalLink){
            if (substr($contentObject->getTypoLink_URL($this->additionalLink), 0, 1) === '/') {
                return rtrim(GeneralUtility::getIndpEnv('TYPO3_SITE_URL'),"/").$contentObject->getTypoLink_URL($this->additionalLink);
            }
        }
        return $this->additionalLink ? $contentObject->getTypoLink_URL($this->additionalLink) : '';
    }

    /**
     * Sets the additionalLink
     *
     * @param string $additionalLink
     * @return void
     */
    public function setAdditionalLink(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $additionalLink)
    {
        $this->additionalLink = $additionalLink;
    }
}