<?php
namespace Netfed\IronStudies\Domain\TypeConverter;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Relation
 */
class RelationConverter extends \TYPO3\CMS\Extbase\Property\TypeConverter\AbstractTypeConverter
{

    /**
     * Actually convert from $source to $targetType, taking into account the fully
     * built $convertedChildProperties and $configuration.
     *
     * @param array $source
     * @param string $targetType
     * @param array $convertedChildProperties
     * @param \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration
     *
     * @return mix
     */
    public function convertFrom($source, $targetType, array $convertedChildProperties = array(), \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration = null)
    {
        $source = is_array($source) ? array_values(array_filter($source)) : $source;
        $targetType = str_replace(['TYPO3\CMS\Extbase\Persistence\ObjectStorage<', '>'], '', $targetType);
        $repository = \TYPO3\CMS\Core\Utility\ClassNamingUtility::translateModelNameToRepositoryName($targetType);
        $object = null;
        if (isset($source['uid']) || isset($source['__identity']) || is_string($source) || is_int($source)) {
            $object = $source['uid'] ?? $source['__identity'] ?? $source ? $this->objectManager->get($repository)->findByUid($source['uid'] ?? $source['__identity'] ?? $source) : $this->objectManager->get($targetType);
        } elseif ($source[0]) {
            $object = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class);
            foreach ($source as $src) {
                $object->attach($this->convertFrom($src, $targetType, $convertedChildProperties, $configuration));
            }
        } else {
            $object = $this->objectManager->get($targetType);
            foreach ($source as $propery => $value) {
                $propertyTargetType = explode('\\', $targetType);
                array_pop($propertyTargetType);
                $propertyTargetType[] = ucfirst($propery);
                $propertyTargetType = implode('\\', $propertyTargetType);

                $object->_setProperty($propery, $this->convertFrom($value, $propertyTargetType, $convertedChildProperties, $configuration));
            }
        }
        return $object;
    }

}
