<?php
namespace Netfed\IronStudies\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Netfed\IronStudies\Controller\StudyController.
 *
 */
class StudyControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \Netfed\IronStudies\Controller\StudyController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('Netfed\\IronStudies\\Controller\\StudyController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllStudiesFromRepositoryAndAssignsThemToView()
	{

		$allStudies = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$studyRepository = $this->getMock('Netfed\\IronStudies\\Domain\\Repository\\StudyRepository', array('findAll'), array(), '', FALSE);
		$studyRepository->expects($this->once())->method('findAll')->will($this->returnValue($allStudies));
		$this->inject($this->subject, 'studyRepository', $studyRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('studies', $allStudies);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenStudyToView()
	{
		$study = new \Netfed\IronStudies\Domain\Model\Study();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('study', $study);

		$this->subject->showAction($study);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenStudyToStudyRepository()
	{
		$study = new \Netfed\IronStudies\Domain\Model\Study();

		$studyRepository = $this->getMock('Netfed\\IronStudies\\Domain\\Repository\\StudyRepository', array('add'), array(), '', FALSE);
		$studyRepository->expects($this->once())->method('add')->with($study);
		$this->inject($this->subject, 'studyRepository', $studyRepository);

		$this->subject->createAction($study);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenStudyToView()
	{
		$study = new \Netfed\IronStudies\Domain\Model\Study();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('study', $study);

		$this->subject->editAction($study);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenStudyInStudyRepository()
	{
		$study = new \Netfed\IronStudies\Domain\Model\Study();

		$studyRepository = $this->getMock('Netfed\\IronStudies\\Domain\\Repository\\StudyRepository', array('update'), array(), '', FALSE);
		$studyRepository->expects($this->once())->method('update')->with($study);
		$this->inject($this->subject, 'studyRepository', $studyRepository);

		$this->subject->updateAction($study);
	}

	/**
	 * @test
	 */
	public function deleteActionRemovesTheGivenStudyFromStudyRepository()
	{
		$study = new \Netfed\IronStudies\Domain\Model\Study();

		$studyRepository = $this->getMock('Netfed\\IronStudies\\Domain\\Repository\\StudyRepository', array('remove'), array(), '', FALSE);
		$studyRepository->expects($this->once())->method('remove')->with($study);
		$this->inject($this->subject, 'studyRepository', $studyRepository);

		$this->subject->deleteAction($study);
	}
}
