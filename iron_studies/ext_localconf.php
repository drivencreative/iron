<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Netfed.' . $_EXTKEY,
	'Ironstudies',
	array(
		'Study' => 'list, show',

	),
	// non-cacheable actions
	array(
		'Study' => '',
	)
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Netfed.' . $_EXTKEY,
	'Form',
	array(
		'Order' => 'process',

	),
	// non-cacheable actions
	array(
        'Order' => 'process',
	)
);
