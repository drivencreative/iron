<?php
namespace Netfed\IronTeammembers\Controller;

/***
 *
 * This file is part of the "Iron Team Members" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * MemberController
 */
class MemberController extends AbstractApiController
{
    /**
     * Configuration for JsonView
     **/
    protected $model = [
        '_exclude' => ['pid'],
    ];

    /**
     * memberRepository
     *
     * @var \Netfed\IronTeammembers\Domain\Repository\MemberRepository
     * @inject
     */
    protected $memberRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        if ($this->request->getFormat()=='json'){
            $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
            $querySettings->setRespectStoragePage(FALSE);
            $this->memberRepository->setDefaultQuerySettings($querySettings);
        }
        $members = $this->memberRepository->findAll();
        $this->view->assign('contentUid', $this->configurationManager->getContentObject()->data['uid']);
        $this->view->assign('data', $members);
    }

    /**
     * action show
     *
     * @param \Netfed\IronTeammembers\Domain\Model\Member $member
     * @return void
     */
    public function showAction(\Netfed\IronTeammembers\Domain\Model\Member $member)
    {
        $this->view->assign('data', $member);
    }

}
