<?php
namespace Netfed\IronTeammembers\Domain\Model;

/***
 *
 * This file is part of the "Iron Team Members" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Member
 */
class Member extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * photo
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $photo = null;

    /**
     * biography
     *
     * @var string
     */
    protected $biography = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * xing
     *
     * @var string
     */
    protected $xing = '';

    /**
     * linkedin
     *
     * @var string
     */
    protected $linkedin = '';

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the photo
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $photo
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Sets the photo
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $photo
     * @return void
     */
    public function setPhoto(\TYPO3\CMS\Extbase\Domain\Model\FileReference $photo)
    {
        $this->photo = $photo;
    }

    /**
     * Returns the biography
     *
     * @return string $biography
     */
    public function getBiography()
    {
        return html_entity_decode($this->biography, ENT_HTML401, 'UTF-8');
    }

    /**
     * Sets the biography
     *
     * @param string $biography
     * @return void
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the xing
     *
     * @return string $xing
     */
    public function getXing()
    {
        return $this->xing;
    }

    /**
     * Sets the xing
     *
     * @param string $xing
     * @return void
     */
    public function setXing($xing)
    {
        $this->xing = $xing;
    }

    /**
     * Returns the linkedin
     *
     * @return string $linkedin
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * Sets the linkedin
     *
     * @param string $linkedin
     * @return void
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;
    }

    /**
     * Image processing
     *
     * @return string
     */
    public function getImage() {
        $settings = [];
        if ($this->photo) {
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
            $configurationManager = $objectManager->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class);
            $settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);

            /** @var $imageService \TYPO3\CMS\Extbase\Service\ImageService **/
            $imageService = $objectManager->get(\TYPO3\CMS\Extbase\Service\ImageService::class);
            try {
                $image = $imageService->getImage($this->photo->getUid(), $this->photo, 0);

                $cropString = $image->hasProperty('crop') ? $image->getProperty('crop') : '';
                $cropVariantCollection = CropVariantCollection::create((string)$cropString);
                $cropVariant = $settings['cropVariant'] ?: 'default';
                $cropArea = $cropVariantCollection->getCropArea($cropVariant);
                $processingInstructions = [
//                    'width' => $this->arguments['width'],
//                    'height' => $this->arguments['height'],
//                    'minWidth' => $this->arguments['minWidth'],
//                    'minHeight' => $this->arguments['minHeight'],
                    'maxWidth' => $settings['width'],
//                    'maxHeight' => $this->arguments['maxHeight'],
                    'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
                ];
                $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
                $imageUri = $imageService->getImageUri($processedImage, 1);

                return $imageUri;
            } catch (\TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException $e) {
                // thrown if file does not exist
            } catch (\UnexpectedValueException $e) {
                // thrown if a file has been replaced with a folder
            } catch (\RuntimeException $e) {
                // RuntimeException thrown if a file is outside of a storage
            } catch (\InvalidArgumentException $e) {
                // thrown if file storage does not exist
            }
        }
    }

}
