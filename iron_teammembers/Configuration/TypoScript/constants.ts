
plugin.tx_ironteammembers_teammembers {
    view {
        # cat=plugin.tx_ironteammembers_teammembers/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:iron_teammembers/Resources/Private/Templates/
        # cat=plugin.tx_ironteammembers_teammembers/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:iron_teammembers/Resources/Private/Partials/
        # cat=plugin.tx_ironteammembers_teammembers/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:iron_teammembers/Resources/Private/Layouts/
    }
}
