
plugin.tx_ironteammembers_teammembers {
    view {
        templateRootPaths.0 = EXT:iron_teammembers/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_ironteammembers_teammembers.view.templateRootPath}
        partialRootPaths.0 = EXT:iron_teammembers/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_ironteammembers_teammembers.view.partialRootPath}
        layoutRootPaths.0 = EXT:iron_teammembers/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_ironteammembers_teammembers.view.layoutRootPath}
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
    settings {
        width = 460
    }
}

api_tag = PAGE
api_tag {
    config {
        disableAllHeaderCode = 1
        debug = 0
        no_cache = 1
        additionalHeaders {
            10 {
                header = Content-Type: application/json
                replace = 1
            }
        }
    }
    typeNum = 19852604
    10 < tt_content.list.20.ironteammembers_teammembers
}