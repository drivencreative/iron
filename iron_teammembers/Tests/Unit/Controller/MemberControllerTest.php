<?php
namespace Netfed\IronTeammembers\Tests\Unit\Controller;

/**
 * Test case.
 */
class MemberControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\IronTeammembers\Controller\MemberController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Netfed\IronTeammembers\Controller\MemberController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllMembersFromRepositoryAndAssignsThemToView()
    {

        $allMembers = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $memberRepository = $this->getMockBuilder(\Netfed\IronTeammembers\Domain\Repository\MemberRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $memberRepository->expects(self::once())->method('findAll')->will(self::returnValue($allMembers));
        $this->inject($this->subject, 'memberRepository', $memberRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('members', $allMembers);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenMemberToView()
    {
        $member = new \Netfed\IronTeammembers\Domain\Model\Member();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('member', $member);

        $this->subject->showAction($member);
    }
}
