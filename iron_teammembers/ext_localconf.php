<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.IronTeammembers',
            'Teammembers',
            [
                'Member' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Member' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    teammembers {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('iron_teammembers') . 'Resources/Public/Icons/user_plugin_teammembers.svg
                        title = LLL:EXT:iron_teammembers/Resources/Private/Language/locallang_db.xlf:tx_iron_teammembers_domain_model_teammembers
                        description = LLL:EXT:iron_teammembers/Resources/Private/Language/locallang_db.xlf:tx_iron_teammembers_domain_model_teammembers.description
                        tt_content_defValues {
                            CType = list
                            list_type = ironteammembers_teammembers
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
