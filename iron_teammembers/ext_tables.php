<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Netfed.IronTeammembers',
            'Teammembers',
            'Team Members'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('iron_teammembers', 'Configuration/TypoScript', 'Iron Team Members');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ironteammembers_domain_model_member', 'EXT:iron_teammembers/Resources/Private/Language/locallang_csh_tx_ironteammembers_domain_model_member.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ironteammembers_domain_model_member');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'iron_teammembers',
            'tx_ironteammembers_domain_model_member'
        );

    }
);
$pluginSignature = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY)) . '_teammembers';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:iron_teammembers/Configuration/FlexForms/flexform.xml'
);