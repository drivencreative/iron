<?php
namespace Netfed\IronTop10deals\Controller;

/***
 *
 * This file is part of the "Top 10 M&amp;A Deals" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * DealController
 */
class DealController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * dealRepository
     *
     * @var \Netfed\IronTop10deals\Domain\Repository\DealRepository
     * @inject
     */
    protected $dealRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $deals = $this->dealRepository->findAll();
        $this->view->assign('deals', $deals);
    }

    /**
     * action list
     *
     * @return void
     */
    public function listFeaturedAction()
    {
        $deals = $this->dealRepository->findByUids($this->settings['selected_records']?$this->settings['selected_records']:3);
        $this->view->assign('deals', $deals);
    }
}
