<?php
namespace Netfed\IronTop10deals\Domain\Model;

/***
 *
 * This file is part of the "Top 10 M&amp;A Deals" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Deal
 */
class Deal extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * dealvalue
     *
     * @var float
     */
    protected $dealvalue = 0.0;

    /**
     * completeddate
     *
     * @var \DateTime
     */
    protected $completeddate = null;

    /**
     * targetname
     *
     * @var string
     */
    protected $targetname = '';

    /**
     * acquirorname
     *
     * @var string
     */
    protected $acquirorname = '';

    /**
     * vendorname
     *
     * @var string
     */
    protected $vendorname = '';

    /**
     * dealtype
     *
     * @var string
     */
    protected $dealtype = '';

    /**
     * targetcountry
     *
     * @var \SJBR\StaticInfoTables\Domain\Model\Country
     */
    protected $targetcountry = null;

    /**
     * acquirorcountry
     *
     * @var \SJBR\StaticInfoTables\Domain\Model\Country
     */
    protected $acquirorcountry = null;

    /**
     * footnote
     *
     * @var string
     */
    protected $footnote = '';

    /**
     * Returns the dealvalue
     *
     * @return float $dealvalue
     */
    public function getDealvalue()
    {
        return $this->dealvalue;
    }

    /**
     * Sets the dealvalue
     *
     * @param float $dealvalue
     * @return void
     */
    public function setDealvalue($dealvalue)
    {
        $this->dealvalue = $dealvalue;
    }

    /**
     * Returns the completeddate
     *
     * @return \DateTime $completeddate
     */
    public function getCompleteddate()
    {
        return $this->completeddate;
    }

    /**
     * Sets the completeddate
     *
     * @param \DateTime $completeddate
     * @return void
     */
    public function setCompleteddate(\DateTime $completeddate)
    {
        $this->completeddate = $completeddate;
    }

    /**
     * Returns the targetname
     *
     * @return string $targetname
     */
    public function getTargetname()
    {
        return $this->targetname;
    }

    /**
     * Sets the targetname
     *
     * @param string $targetname
     * @return void
     */
    public function setTargetname($targetname)
    {
        $this->targetname = $targetname;
    }

    /**
     * Returns the acquirorname
     *
     * @return string $acquirorname
     */
    public function getAcquirorname()
    {
        return $this->acquirorname;
    }

    /**
     * Sets the acquirorname
     *
     * @param string $acquirorname
     * @return void
     */
    public function setAcquirorname($acquirorname)
    {
        $this->acquirorname = $acquirorname;
    }

    /**
     * Returns the vendorname
     *
     * @return string $vendorname
     */
    public function getVendorname()
    {
        return $this->vendorname;
    }

    /**
     * Sets the vendorname
     *
     * @param string $vendorname
     * @return void
     */
    public function setVendorname($vendorname)
    {
        $this->vendorname = $vendorname;
    }

    /**
     * Returns the dealtype
     *
     * @return string $dealtype
     */
    public function getDealtype()
    {
        return $this->dealtype;
    }

    /**
     * Sets the dealtype
     *
     * @param string $dealtype
     * @return void
     */
    public function setDealtype($dealtype)
    {
        $this->dealtype = $dealtype;
    }

    /**
     * Returns the targetcountry
     *
     * @return \SJBR\StaticInfoTables\Domain\Model\Country $targetcountry
     */
    public function getTargetcountry()
    {
        return $this->targetcountry;
    }

    /**
     * Sets the targetcountry
     *
     * @param \SJBR\StaticInfoTables\Domain\Model\Country $targetcountry
     * @return void
     */
    public function setTargetcountry(\SJBR\StaticInfoTables\Domain\Model\Country $targetcountry)
    {
        $this->targetcountry = $targetcountry;
    }

    /**
     * Returns the acquirorcountry
     *
     * @return \SJBR\StaticInfoTables\Domain\Model\Country $acquirorcountry
     */
    public function getAcquirorcountry()
    {
        return $this->acquirorcountry;
    }

    /**
     * Sets the acquirorcountry
     *
     * @param \SJBR\StaticInfoTables\Domain\Model\Country $acquirorcountry
     * @return void
     */
    public function setAcquirorcountry(\SJBR\StaticInfoTables\Domain\Model\Country $acquirorcountry)
    {
        $this->acquirorcountry = $acquirorcountry;
    }

    /**
     * Returns string footnote
     *
     * @return string $footnote
     */
    public function getFootnote()
    {
        return $this->footnote;
    }

    /**
     * Sets the footnote
     *
     * @param string $footnote
     * @return void
     */
    public function setFootnote($footnote)
    {
        $this->footnote = $footnote;
    }
}
