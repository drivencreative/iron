<?php
namespace Netfed\IronTop10deals\Domain\Repository;

/***
 *
 * This file is part of the "Top 10 M&amp;A Deals" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Deals
 */
class DealRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'dealvalue' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
    ];

    /**
     * get Selected records
     *
     * @param string String containing uids
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByUids($uids) {
        $uidArray = explode(",", $uids);
        $query = $this->createQuery();

        return $query->matching(
            $query->logicalAnd(
                $query->logicalOr(
                    $query->in('uid', $uidArray),
                    $query->in('l10n_parent', $uidArray)
                ),
                $query->equals('hidden', 0),
                $query->equals('deleted', 0)
            )
        )->execute();
    }
}
