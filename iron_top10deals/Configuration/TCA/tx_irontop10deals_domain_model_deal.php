<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_irontop10deals_domain_model_deal',
        'label' => 'dealvalue',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'dealvalue,completeddate,targetname,acquirorname,vendorname,dealtype,targetcountry,acquirorcountry,footnote',
        'iconfile' => 'EXT:iron_top10deals/Resources/Public/Icons/tx_irontop10deals_domain_model_deal.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, dealvalue, completeddate, targetname, acquirorname, vendorname, dealtype, targetcountry, acquirorcountry, footnote',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, dealvalue, completeddate, targetname, acquirorname, vendorname, dealtype, targetcountry, acquirorcountry, footnote, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_irontop10deals_domain_model_deal',
                'foreign_table_where' => 'AND tx_irontop10deals_domain_model_deal.pid=###CURRENT_PID### AND tx_irontop10deals_domain_model_deal.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'dealvalue' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_irontop10deals_domain_model_deal.dealvalue',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2'
            ]
        ],
        'completeddate' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_irontop10deals_domain_model_deal.completeddate',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'datetime',
                'default' => time()
            ],
        ],
        'targetname' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_irontop10deals_domain_model_deal.targetname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'acquirorname' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_irontop10deals_domain_model_deal.acquirorname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'vendorname' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_irontop10deals_domain_model_deal.vendorname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'dealtype' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_irontop10deals_domain_model_deal.dealtype',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'targetcountry' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_irontop10deals_domain_model_deal.targetcountry',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'static_countries',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'acquirorcountry' => [
            'exclude' => true,
            'label' => 'LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_irontop10deals_domain_model_deal.acquirorcountry',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'static_countries',
                'items' => [
                    ['None', 0],
                ],
                'default' => 0,
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'footnote' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_irontop10deals_domain_model_deal.footnote',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Select footnote --', ''],
                    ['Single footnote', '*'],
                    ['Double footnote', '**'],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
    ],
];
