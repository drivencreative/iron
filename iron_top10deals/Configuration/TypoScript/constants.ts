
plugin.tx_irontop10deals_top10dealsfull {
    view {
        # cat=plugin.tx_irontop10deals_top10dealsfull/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:iron_top10deals/Resources/Private/Templates/
        # cat=plugin.tx_irontop10deals_top10dealsfull/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:iron_top10deals/Resources/Private/Partials/
        # cat=plugin.tx_irontop10deals_top10dealsfull/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:iron_top10deals/Resources/Private/Layouts/
    }
}

plugin.tx_irontop10deals_top10dealsfeatured {
    view {
        # cat=plugin.tx_irontop10deals_top10dealsfeatured/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:iron_top10deals/Resources/Private/Templates/
        # cat=plugin.tx_irontop10deals_top10dealsfeatured/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:iron_top10deals/Resources/Private/Partials/
        # cat=plugin.tx_irontop10deals_top10dealsfeatured/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:iron_top10deals/Resources/Private/Layouts/
    }
}
