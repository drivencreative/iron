
plugin.tx_irontop10deals_top10dealsfull {
    view {
        templateRootPaths.0 = EXT:iron_top10deals/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_irontop10deals_top10dealsfull.view.templateRootPath}
        partialRootPaths.0 = EXT:iron_top10deals/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_irontop10deals_top10dealsfull.view.partialRootPath}
        layoutRootPaths.0 = EXT:iron_top10deals/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_irontop10deals_top10dealsfull.view.layoutRootPath}
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_irontop10deals_top10dealsfeatured {
    view {
        templateRootPaths.0 = EXT:iron_top10deals/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_irontop10deals_top10dealsfeatured.view.templateRootPath}
        partialRootPaths.0 = EXT:iron_top10deals/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_irontop10deals_top10dealsfeatured.view.partialRootPath}
        layoutRootPaths.0 = EXT:iron_top10deals/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_irontop10deals_top10dealsfeatured.view.layoutRootPath}
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

