<?php
namespace Netfed\IronTop10deals\Tests\Unit\Controller;

/**
 * Test case.
 */
class DealControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\IronTop10deals\Controller\DealController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Netfed\IronTop10deals\Controller\DealController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllDealsFromRepositoryAndAssignsThemToView()
    {

        $allDeals = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $dealRepository = $this->getMockBuilder(\Netfed\IronTop10deals\Domain\Repository\DealRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $dealRepository->expects(self::once())->method('findAll')->will(self::returnValue($allDeals));
        $this->inject($this->subject, 'dealRepository', $dealRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('deals', $allDeals);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
