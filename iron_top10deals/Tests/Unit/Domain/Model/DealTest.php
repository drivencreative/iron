<?php
namespace Netfed\IronTop10deals\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class DealTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\IronTop10deals\Domain\Model\Deal
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\IronTop10deals\Domain\Model\Deal();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getDealvalueReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getDealvalue()
        );
    }

    /**
     * @test
     */
    public function setDealvalueForFloatSetsDealvalue()
    {
        $this->subject->setDealvalue(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'dealvalue',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getCompleteddateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getCompleteddate()
        );
    }

    /**
     * @test
     */
    public function setCompleteddateForDateTimeSetsCompleteddate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setCompleteddate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'completeddate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTargetnameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTargetname()
        );
    }

    /**
     * @test
     */
    public function setTargetnameForStringSetsTargetname()
    {
        $this->subject->setTargetname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'targetname',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAcquirornameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAcquirorname()
        );
    }

    /**
     * @test
     */
    public function setAcquirornameForStringSetsAcquirorname()
    {
        $this->subject->setAcquirorname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'acquirorname',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getVendornameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getVendorname()
        );
    }

    /**
     * @test
     */
    public function setVendornameForStringSetsVendorname()
    {
        $this->subject->setVendorname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'vendorname',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDealtypeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDealtype()
        );
    }

    /**
     * @test
     */
    public function setDealtypeForStringSetsDealtype()
    {
        $this->subject->setDealtype('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'dealtype',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTargetcountryReturnsInitialValueForCountry()
    {
        self::assertEquals(
            null,
            $this->subject->getTargetcountry()
        );
    }

    /**
     * @test
     */
    public function setTargetcountryForCountrySetsTargetcountry()
    {
        $targetcountryFixture = new \Netfed\IronTop10deals\Domain\Model\Country();
        $this->subject->setTargetcountry($targetcountryFixture);

        self::assertAttributeEquals(
            $targetcountryFixture,
            'targetcountry',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAcquirorcountryReturnsInitialValueForCountry()
    {
        self::assertEquals(
            null,
            $this->subject->getAcquirorcountry()
        );
    }

    /**
     * @test
     */
    public function setAcquirorcountryForCountrySetsAcquirorcountry()
    {
        $acquirorcountryFixture = new \Netfed\IronTop10deals\Domain\Model\Country();
        $this->subject->setAcquirorcountry($acquirorcountryFixture);

        self::assertAttributeEquals(
            $acquirorcountryFixture,
            'acquirorcountry',
            $this->subject
        );
    }
}
