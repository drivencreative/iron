<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.IronTop10deals',
            'Top10dealsfull',
            [
                'Deal' => 'list'
            ],
            // non-cacheable actions
            [
                'Deal' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.IronTop10deals',
            'Top10dealsfeatured',
            [
                'Deal' => 'listFeatured'
            ],
            // non-cacheable actions
            [
                'Deal' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    top10dealsfull {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('iron_top10deals') . 'Resources/Public/Icons/user_plugin_top10dealsfull.svg
                        title = LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_iron_top10deals_domain_model_top10deals.full
                        description = LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_iron_top10deals_domain_model_top10dealsfull.description
                        tt_content_defValues {
                            CType = list
                            list_type = irontop10deals_top10dealsfull
                        }
                    }
                    top10dealsfeatured {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('iron_top10deals') . 'Resources/Public/Icons/user_plugin_top10dealsfeatured.svg
                        title = LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_iron_top10deals_domain_model_top10deals.featured
                        description = LLL:EXT:iron_top10deals/Resources/Private/Language/locallang_db.xlf:tx_iron_top10deals_domain_model_top10dealsfeatured.description
                        tt_content_defValues {
                            CType = list
                            list_type = irontop10deals_top10dealsfeatured
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
