<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Netfed.IronTop10deals',
            'Top10dealsfull',
            'Top 10 M&amp;A Deals - Full List'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Netfed.IronTop10deals',
            'Top10dealsfeatured',
            'Top 10 M&amp;A Deals - Featured List'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('iron_top10deals', 'Configuration/TypoScript', 'Top 10 M&A Deals');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_irontop10deals_domain_model_deal', 'EXT:iron_top10deals/Resources/Private/Language/locallang_csh_tx_irontop10deals_domain_model_deal.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_irontop10deals_domain_model_deal');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_irontop10deals_domain_model_country', 'EXT:iron_top10deals/Resources/Private/Language/locallang_csh_tx_irontop10deals_domain_model_country.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_irontop10deals_domain_model_country');

    }
);
$pluginSignature = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY)) . '_top10dealsfeatured';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:iron_top10deals/Configuration/FlexForms/flexform_featured.xml'
);
$pluginSignature = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY)) . '_top10dealsfull';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:iron_top10deals/Configuration/FlexForms/flexform_full.xml'
);