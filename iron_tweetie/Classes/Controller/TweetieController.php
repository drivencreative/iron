<?php
namespace Netfed\IronTweetie\Controller;

/***
 *
 * This file is part of the "Iron Tweetie" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * TweetieController
 */
class TweetieController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * Tweet
     *
     * @var \Netfed\IronTweetie\Utilities\Tweetie\Tweet
     * @inject
     */
    protected $tweetieUtility = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        if ($this->settings['consumerKey'] != '' && $this->settings['consumerSecret'] != '' && $this->settings['accessToken'] != '' && $this->settings['accessSecret'] != ''){
            $tweeties = $this->tweetieUtility->executeTweets(
                $this->settings
            );
            if($this->settings['hashtag'] != ''){
                $tweeties = $tweeties['statuses'];
            }
            $this->view->assign('tweeties', $tweeties);
        }
    }
}
