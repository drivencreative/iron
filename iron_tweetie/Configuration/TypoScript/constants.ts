
plugin.tx_irontweetie_tweetie {
    view {
        # cat=plugin.tx_irontweetie_tweetie/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:iron_tweetie/Resources/Private/Templates/
        # cat=plugin.tx_irontweetie_tweetie/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:iron_tweetie/Resources/Private/Partials/
        # cat=plugin.tx_irontweetie_tweetie/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:iron_tweetie/Resources/Private/Layouts/
    }
}
