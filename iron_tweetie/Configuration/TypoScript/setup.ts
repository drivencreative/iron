
plugin.tx_irontweetie_tweetie {
    view {
        templateRootPaths.0 = EXT:iron_tweetie/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_irontweetie_tweetie.view.templateRootPath}
        partialRootPaths.0 = EXT:iron_tweetie/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_irontweetie_tweetie.view.partialRootPath}
        layoutRootPaths.0 = EXT:iron_tweetie/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_irontweetie_tweetie.view.layoutRootPath}
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}