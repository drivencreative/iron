<?php
namespace Netfed\IronTweetie\Tests\Unit\Controller;

/**
 * Test case.
 */
class TweetieControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\IronTweetie\Controller\TweetieController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Netfed\IronTweetie\Controller\TweetieController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllTweetiesFromRepositoryAndAssignsThemToView()
    {

        $allTweeties = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tweetieRepository = $this->getMockBuilder(\Netfed\IronTweetie\Domain\Repository\TweetieRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $tweetieRepository->expects(self::once())->method('findAll')->will(self::returnValue($allTweeties));
        $this->inject($this->subject, 'tweetieRepository', $tweetieRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('tweeties', $allTweeties);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenTweetieToView()
    {
        $tweetie = new \Netfed\IronTweetie\Domain\Model\Tweetie();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('tweetie', $tweetie);

        $this->subject->showAction($tweetie);
    }
}
