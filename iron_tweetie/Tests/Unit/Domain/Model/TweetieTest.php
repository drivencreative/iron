<?php
namespace Netfed\IronTweetie\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class TweetieTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\IronTweetie\Domain\Model\Tweetie
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\IronTweetie\Domain\Model\Tweetie();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
