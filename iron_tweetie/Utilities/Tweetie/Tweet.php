<?php

namespace Netfed\IronTweetie\Utilities\Tweetie;


class Tweet
{

    /**
     * @param $consumerKey
     * @param $consumerSecret
     * @param $accessToken
     * @param $accessSecret
     * @return mixed|API|string
     */
    public function executeTweets($settings)
    {
//        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($settings, 'My $settings');die;

        // Check if keys are in place
        if ($settings['consumerKey'] === '' || $settings['consumerSecret'] === '') {
            return 'You need a consumer key and secret keys. Get one from <a href="https://apps.twitter.com/">apps.twitter.com</a>';
        }
        // Connect
        $connection = new TwitterOAuth($settings['consumerKey'] , $settings['consumerSecret'], $settings['accessToken'], $settings['accessSecret']);

        // Get Tweets
        if (!empty($settings['list_slug'])) {
            $params = array(
                'owner_screen_name' => $settings['username'],
                'slug' => $settings['list_slug'],
                'per_page' =>  $settings['number']
            );

            $url = '/lists/statuses';
        } else if ( $settings['hashtag']) {
            $params = array(
                'count' =>  $settings['number'],
                'q' => '#' .  $settings['hashtag']
            );

            $url = '/search/tweets';
        } else {
            $params = array(
                'count' =>  $settings['number'],
                'exclude_replies' => $settings['exclude_replies'],
                'screen_name' => $settings['username']
            );

            $url = '/statuses/user_timeline';
        }

        $tweets = $connection->get($url, $params);
        // Return JSON Object
//        header('Content-Type: application/json');
//        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($tweets, 'tweets');

        return $tweets;
    }
}


