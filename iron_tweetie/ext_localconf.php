<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.IronTweetie',
            'Tweetie',
            [
                'Tweetie' => 'list'
            ],
            // non-cacheable actions
            [
                'Tweetie' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    tweetie {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('iron_tweetie') . 'Resources/Public/Icons/user_plugin_tweetie.svg
                        title = LLL:EXT:iron_tweetie/Resources/Private/Language/locallang_db.xlf:tx_iron_tweetie_domain_model_tweetie
                        description = LLL:EXT:iron_tweetie/Resources/Private/Language/locallang_db.xlf:tx_iron_tweetie_domain_model_tweetie.description
                        tt_content_defValues {
                            CType = list
                            list_type = irontweetie_tweetie
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
