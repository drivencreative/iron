<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Netfed.IronTweetie',
            'Tweetie',
            'Iron Tweetie'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('iron_tweetie', 'Configuration/TypoScript', 'Iron Tweetie');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_irontweetie_domain_model_tweetie', 'EXT:iron_tweetie/Resources/Private/Language/locallang_csh_tx_irontweetie_domain_model_tweetie.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_irontweetie_domain_model_tweetie');

    }
);
$pluginSignature = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY)) . '_tweetie';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:iron_tweetie/Configuration/FlexForms/flexform.xml'
);