<?php
namespace Netfed\IronViewpoints\Controller;

/***
 *
 * This file is part of the "Iron Points of View" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * PeriodController
 */
class PeriodController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * periodRepository
     *
     * @var \Netfed\IronViewpoints\Domain\Repository\PeriodRepository
     * @inject
     */
    protected $periodRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $periods = $this->periodRepository->findAll();
        $this->view->assign('periods', $periods);
    }
}
