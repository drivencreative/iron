<?php

namespace Netfed\IronViewpoints\Controller;

/***
 *
 * This file is part of the "Iron Points of View" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * ViewPointController
 */
class ViewPointController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * viewPointRepository
     *
     * @var \Netfed\IronViewpoints\Domain\Repository\ViewPointRepository
     * @inject
     */
    protected $viewPointRepository = null;

    /**
     * periodRepository
     *
     * @var \Netfed\IronViewpoints\Domain\Repository\PeriodRepository
     * @inject
     */
    protected $periodRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $viewPoints = $this->viewPointRepository->findAllLatestLimited((int)$this->settings['viewpoints']['max'], (int)$this->settings['sorting_latest']);
        $viewPointsAll = $this->viewPointRepository->findAll();
        $periods = $this->periodRepository->findAll();
        $this->view->assign('periods', $periods);

        $this->view->assign('viewPointsAll', $viewPointsAll);
        $this->view->assign('viewPoints', $viewPoints);
        $this->view->assign('contentUid', $this->configurationManager->getContentObject()->data['uid']);
    }

    /**
     * action listAll
     *
     * @return void
     */
    public function listAllAction()
    {
        $viewPoints = $this->viewPointRepository->findAll();
//        DebuggerUtility::var_dump($viewPoints);die;
        $this->view->assign('viewPoints', $viewPoints);
        $this->view->assign('contentUid', $this->configurationManager->getContentObject()->data['uid']);
    }

    /**
     * action listFurther
     *
     * @return void
     */
    public function listFurtherAction()
    {
        $listFurtherViewPoints = $this->viewPointRepository->findByUids($this->settings['selected_records']?$this->settings['selected_records']:3);
        $this->view->assign('listFurtherViewPoints', $listFurtherViewPoints);
        $this->view->assign('contentUid', $this->configurationManager->getContentObject()->data['uid']);
    }

    /**
     * action listLatest
     *
     * @return void
     */
    public function listLatestAction()
    {

        $viewPointsLatestLimited = $this->viewPointRepository->findAllLatestLimited((int)$this->settings['viewpoints']['max'], $this->settings['sorting_latest']);
        $this->view->assign('viewPointsLatestLimited', $viewPointsLatestLimited);
        $this->view->assign('contentUid', $this->configurationManager->getContentObject()->data['uid']);
    }

    /**
     * action show
     *
     * @param \Netfed\IronViewpoints\Domain\Model\ViewPoint $viewPoint
     * @return void
     */
    public function showAction(\Netfed\IronViewpoints\Domain\Model\ViewPoint $viewPoint)
    {
        $listFurtherViewPoints = $this->viewPointRepository->findByUids($this->settings['selected_records']?$this->settings['selected_records']:3);
        $this->view->assign('viewPoint', $viewPoint);
        $this->view->assign('listFurtherViewPoints', $listFurtherViewPoints);
        $this->view->assign('contentUid', $this->configurationManager->getContentObject()->data['uid']);
    }
}
