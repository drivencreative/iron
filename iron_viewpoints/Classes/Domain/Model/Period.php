<?php
namespace Netfed\IronViewpoints\Domain\Model;

/***
 *
 * This file is part of the "Iron Points of View" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Period
 */
class Period extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * start
     *
     * @var \DateTime
     */
    protected $start = null;

    /**
     * end
     *
     * @var \DateTime
     */
    protected $end = null;

    /**
     * viewpoints
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\IronViewpoints\Domain\Model\ViewPoint>
     * @cascade remove
     */
    protected $viewpoints = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->viewpoints = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the start
     *
     * @return \DateTime $start
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Sets the start
     *
     * @param \DateTime $start
     * @return void
     */
    public function setStart(\DateTime $start)
    {
        $this->start = $start;
    }

    /**
     * Returns the end
     *
     * @return \DateTime $end
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Sets the end
     *
     * @param \DateTime $end
     * @return void
     */
    public function setEnd(\DateTime $end)
    {
        $this->end = $end;
    }

    /**
     * Adds a ViewPoint
     *
     * @param \Netfed\IronViewpoints\Domain\Model\ViewPoint $viewpoint
     * @return void
     */
    public function addViewpoint(\Netfed\IronViewpoints\Domain\Model\ViewPoint $viewpoint)
    {
        $this->viewpoints->attach($viewpoint);
    }

    /**
     * Removes a ViewPoint
     *
     * @param \Netfed\IronViewpoints\Domain\Model\ViewPoint $viewpointToRemove The ViewPoint to be removed
     * @return void
     */
    public function removeViewpoint(\Netfed\IronViewpoints\Domain\Model\ViewPoint $viewpointToRemove)
    {
        $this->viewpoints->detach($viewpointToRemove);
    }

    /**
     * Returns the viewpoints
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\IronViewpoints\Domain\Model\ViewPoint> $viewpoints
     */
    public function getViewpoints()
    {
        return $this->viewpoints;
    }

    /**
     * Sets the viewpoints
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\IronViewpoints\Domain\Model\ViewPoint> $viewpoints
     * @return void
     */
    public function setViewpoints(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $viewpoints)
    {
        $this->viewpoints = $viewpoints;
    }
}
