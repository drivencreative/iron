<?php
namespace Netfed\IronViewpoints\Domain\Model;

/***
 *
 * This file is part of the "Iron Points of View" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * ViewPoint
 */
class ViewPoint extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * teaser
     *
     * @var string
     */
    protected $teaser = '';

    /**
     * files
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @lazy
     */
    protected $files = null;

    /**
     * archived
     *
     * @var bool
     */
    protected $archived = false;

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * intro
     *
     * @var string
     */
    protected $intro = '';

    /**
     * text
     *
     * @var string
     */
    protected $text = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * additionalText
     *
     * @var string
     */
    protected $additionalText = '';

    /**
     * date
     *
     * @var \DateTime
     */
    protected $date = null;

    /**
     * related
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\IronViewpoints\Domain\Model\ViewPoint>
     */
    protected $related = null;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the intro
     *
     * @return string $intro
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Sets the intro
     *
     * @param string $intro
     * @return void
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the additionalText
     *
     * @return string $additionalText
     */
    public function getAdditionalText()
    {
        return $this->additionalText;
    }

    /**
     * Sets the additionalText
     *
     * @param string $additionalText
     * @return void
     */
    public function setAdditionalText($additionalText)
    {
        $this->additionalText = $additionalText;
    }

    /**
     * Returns the teaser
     *
     * @return string $teaser
     */
    public function getTeaser()
    {
        return $this->teaser;
    }

    /**
     * Sets the teaser
     *
     * @param string $teaser
     * @return void
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
    }

    /**
     * Returns the related
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\IronViewpoints\Domain\Model\ViewPoint> $related
     */
    public function getRelated()
    {
        return $this->related;
    }

    /**
     * Sets the related
     *
     * @param \Netfed\Viewpoints\Domain\Model\ViewPoint $related
     * @return void
     */
    public function setRelated(\Netfed\Viewpoints\Domain\Model\ViewPoint $related)
    {
        $this->related = $related;
    }

    /**
     * Returns the date
     *
     * @return string $date
     */
    public function getDateGroup()
    {
        $date = $this->date ? $this->date->format('Y') : 0;
        if ($date % 2) {
            return $date . ' - ' . ($date - 1);
        }
        return $date + 1 . ' - ' . $date;
    }

    /**
     * Returns the date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the date
     *
     * @param \DateTime $date
     * @return void
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Returns the files
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $files
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Sets the files
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $files
     * @return void
     */
    public function setFiles(\TYPO3\CMS\Extbase\Domain\Model\FileReference $files)
    {
        $this->files = $files;
    }

    /**
     * Adds a files
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $files
     * @return void
     */
    public function addPictures(\TYPO3\CMS\Extbase\Domain\Model\FileReference $files)
    {
        $this->files->attach($files);
    }

    /**
     * Removes a files
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $files The Travel to be removed
     * @return void
     */
    public function removePictures(\TYPO3\CMS\Extbase\Domain\Model\FileReference $files)
    {
        $this->files->detach($files);
    }

    /**
     * Returns the archived
     *
     * @return bool $archived
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * Sets the archived
     *
     * @param bool $archived
     * @return void
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
    }

    /**
     * Returns the boolean state of archived
     *
     * @return bool
     */
    public function isArchived()
    {
        return $this->archived;
    }
}
