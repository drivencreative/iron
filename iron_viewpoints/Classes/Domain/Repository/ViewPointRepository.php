<?php
namespace Netfed\IronViewpoints\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryInterface;
/***
 *
 * This file is part of the "Iron Points of View" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for ViewPoints
 */
class ViewPointRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * get Limited amount of records
     *
     * @param $limit
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllLimited($limit = 1)
    {
        $query = $this->createQuery();
        return $query->matching($query->equals('archived', 0))->setLimit($limit)->execute();
    }

    /**
     * get Archived records
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findArchived()
    {
        $query = $this->createQuery();
        $query->matching($query->equals('archived', 1));
        return $query->execute();
    }

    /**
     * get Latest amount of records
     *
     * @param $limit
     * @param $sorting
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllLatestLimited($limit = 6, $sorting = false)
    {
        $query = $this->createQuery();
        $query->matching($query->equals('archived', 0));
        $query->setOrderings(['date' => $sorting ? QueryInterface::ORDER_DESCENDING : QueryInterface::ORDER_ASCENDING]);
        return $query->setLimit($limit)->execute();
    }

    /**
     * get Selected records
     *
     * @param string String containing uids
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByUids($uids)
    {
        $uidArray = explode(',', $uids);
        $query = $this->createQuery();
        foreach ($uidArray as $key => $value) {
            $constraints[] = $query->equals('uid', $value);
        }
        return $query->matching($query->logicalAnd($query->logicalOr($constraints), $query->equals('hidden', 0), $query->equals('deleted', 0)))->execute();
    }
}
