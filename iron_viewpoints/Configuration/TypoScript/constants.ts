
plugin.tx_ironviewpoints_iron_viewpoints {
    view {
        # cat=plugin.tx_ironviewpoints_iron_viewpoints/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:iron_viewpoints/Resources/Private/Templates/
        # cat=plugin.tx_ironviewpoints_iron_viewpoints/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:iron_viewpoints/Resources/Private/Partials/
        # cat=plugin.tx_ironviewpoints_iron_viewpoints/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:iron_viewpoints/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_ironviewpoints_iron_viewpoints//a; type=string; label=Default storage PID
        storagePid =
    }
}
