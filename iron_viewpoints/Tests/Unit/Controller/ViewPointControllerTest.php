<?php
namespace Netfed\IronViewpoints\Tests\Unit\Controller;

/**
 * Test case.
 */
class ViewPointControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\IronViewpoints\Controller\ViewPointController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Netfed\IronViewpoints\Controller\ViewPointController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllViewPointsFromRepositoryAndAssignsThemToView()
    {

        $allViewPoints = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $viewPointRepository = $this->getMockBuilder(\Netfed\IronViewpoints\Domain\Repository\ViewPointRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $viewPointRepository->expects(self::once())->method('findAll')->will(self::returnValue($allViewPoints));
        $this->inject($this->subject, 'viewPointRepository', $viewPointRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('viewPoints', $allViewPoints);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenViewPointToView()
    {
        $viewPoint = new \Netfed\IronViewpoints\Domain\Model\ViewPoint();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('viewPoint', $viewPoint);

        $this->subject->showAction($viewPoint);
    }
}
