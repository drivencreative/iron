<?php
namespace Netfed\IronViewpoints\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class PeriodTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\IronViewpoints\Domain\Model\Period
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\IronViewpoints\Domain\Model\Period();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getStartReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getStart()
        );
    }

    /**
     * @test
     */
    public function setStartForDateTimeSetsStart()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setStart($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'start',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEndReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getEnd()
        );
    }

    /**
     * @test
     */
    public function setEndForDateTimeSetsEnd()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setEnd($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'end',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getViewpoointsReturnsInitialValueForViewPoint()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getViewpooints()
        );
    }

    /**
     * @test
     */
    public function setViewpoointsForObjectStorageContainingViewPointSetsViewpooints()
    {
        $viewpooint = new \Netfed\IronViewpoints\Domain\Model\ViewPoint();
        $objectStorageHoldingExactlyOneViewpooints = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneViewpooints->attach($viewpooint);
        $this->subject->setViewpooints($objectStorageHoldingExactlyOneViewpooints);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneViewpooints,
            'viewpooints',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addViewpoointToObjectStorageHoldingViewpooints()
    {
        $viewpooint = new \Netfed\IronViewpoints\Domain\Model\ViewPoint();
        $viewpoointsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $viewpoointsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($viewpooint));
        $this->inject($this->subject, 'viewpooints', $viewpoointsObjectStorageMock);

        $this->subject->addViewpooint($viewpooint);
    }

    /**
     * @test
     */
    public function removeViewpoointFromObjectStorageHoldingViewpooints()
    {
        $viewpooint = new \Netfed\IronViewpoints\Domain\Model\ViewPoint();
        $viewpoointsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $viewpoointsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($viewpooint));
        $this->inject($this->subject, 'viewpooints', $viewpoointsObjectStorageMock);

        $this->subject->removeViewpooint($viewpooint);
    }
}
