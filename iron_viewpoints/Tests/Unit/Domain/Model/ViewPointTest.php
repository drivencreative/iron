<?php
namespace Netfed\IronViewpoints\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ViewPointTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\IronViewpoints\Domain\Model\ViewPoint
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\IronViewpoints\Domain\Model\ViewPoint();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIntroReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIntro()
        );
    }

    /**
     * @test
     */
    public function setIntroForStringSetsIntro()
    {
        $this->subject->setIntro('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'intro',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText()
        );
    }

    /**
     * @test
     */
    public function setTextForStringSetsText()
    {
        $this->subject->setText('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAdditionalTextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAdditionalText()
        );
    }

    /**
     * @test
     */
    public function setAdditionalTextForStringSetsAdditionalText()
    {
        $this->subject->setAdditionalText('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'additionalText',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDate()
        );
    }

    /**
     * @test
     */
    public function setDateForDateTimeSetsDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'date',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRelatedReturnsInitialValueForViewPoint()
    {
        self::assertEquals(
            null,
            $this->subject->getRelated()
        );
    }

    /**
     * @test
     */
    public function setRelatedForViewPointSetsRelated()
    {
        $relatedFixture = new \Netfed\IronViewpoints\Domain\Model\ViewPoint();
        $this->subject->setRelated($relatedFixture);

        self::assertAttributeEquals(
            $relatedFixture,
            'related',
            $this->subject
        );
    }
}
