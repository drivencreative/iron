<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.IronViewpoints',
            'Viewpoints',
            [
                'ViewPoint' => 'list, show'
            ],
            // non-cacheable actions
            [
                'ViewPoint' => ''
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.IronViewpoints',
            'ViewpointsFeatured',
            [
                'ViewPoint' => 'listLatest, show'
            ],
            // non-cacheable actions
            [
                'ViewPoint' => ''
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.IronViewpoints',
            'ViewpointsLatest',
            [
                'ViewPoint' => 'listLatest, show'
            ],
            // non-cacheable actions
            [
                'ViewPoint' => ''
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.IronViewpoints',
            'ViewpointsFurther',
            [
                'ViewPoint' => 'listFurther, show'
            ],
            // non-cacheable actions
            [
                'ViewPoint' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    iron_viewpoints {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('iron_viewpoints') . 'Resources/Public/Icons/user_plugin_iron_viewpoints.svg
                        title = LLL:EXT:iron_viewpoints/Resources/Private/Language/locallang_db.xlf:tx_iron_viewpoints_domain_model_iron_viewpoints
                        description = LLL:EXT:iron_viewpoints/Resources/Private/Language/locallang_db.xlf:tx_iron_viewpoints_domain_model_iron_viewpoints.description
                        tt_content_defValues {
                            CType = list
                            list_type = ironviewpoints_ironviewpoints
                        }
                    },
                    iron_viewpoints_featured {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('iron_viewpoints') . 'Resources/Public/Icons/user_plugin_iron_viewpoints.svg
                        title = LLL:EXT:iron_viewpoints/Resources/Private/Language/locallang_db.xlf:tx_iron_viewpoints_domain_model_iron_viewpoints.featured
                        description = LLL:EXT:iron_viewpoints/Resources/Private/Language/locallang_db.xlf:tx_iron_viewpoints_domain_model_iron_viewpoints.description
                        tt_content_defValues {
                            CType = list
                            list_type = ironviewpoints_ironviewpoints
                        }
                    }
                    iron_viewpoints_latest {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('iron_viewpoints') . 'Resources/Public/Icons/user_plugin_iron_viewpoints.svg
                        title = LLL:EXT:iron_viewpoints/Resources/Private/Language/locallang_db.xlf:tx_iron_viewpoints_domain_model_iron_viewpoints.latest
                        description = LLL:EXT:iron_viewpoints/Resources/Private/Language/locallang_db.xlf:tx_iron_viewpoints_domain_model_iron_viewpoints.description
                        tt_content_defValues {
                            CType = list
                            list_type = ironviewpoints_ironviewpoints
                        }
                    }
                    iron_viewpoints_further {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('iron_viewpoints') . 'Resources/Public/Icons/user_plugin_iron_viewpoints.svg
                        title = LLL:EXT:iron_viewpoints/Resources/Private/Language/locallang_db.xlf:tx_iron_viewpoints_domain_model_iron_viewpoints.further
                        description = LLL:EXT:iron_viewpoints/Resources/Private/Language/locallang_db.xlf:tx_iron_viewpoints_domain_model_iron_viewpoints.description
                        tt_content_defValues {
                            CType = list
                            list_type = ironviewpoints_ironviewpoints
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
