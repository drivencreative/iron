<?php
defined('TYPO3_MODE') || die('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Netfed.'. $_EXTKEY,
    'Viewpoints',
    'Points of View'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Netfed.'. $_EXTKEY,
    'ViewpointsLatest',
    'Latest Points of View'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Netfed.'. $_EXTKEY,
    'ViewpointsFurther',
    'Further Points of View'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Iron Points of View');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ironviewpoints_domain_model_viewpoint', 'EXT:iron_viewpoints/Resources/Private/Language/locallang_csh_tx_ironviewpoints_domain_model_viewpoint.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ironviewpoints_domain_model_viewpoint');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
    $_EXTKEY,
    'tx_ironviewpoints_domain_model_viewpoint'
);
$pluginSignature = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY)) . '_viewpoints';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:iron_viewpoints/Configuration/FlexForms/flexform.xml'
);
$pluginSignature = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY)) . '_viewpointslatest';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:iron_viewpoints/Configuration/FlexForms/flexform_latest.xml'
);
$pluginSignature = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY)) . '_viewpointsfurther';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:iron_viewpoints/Configuration/FlexForms/flexform_further.xml'
);